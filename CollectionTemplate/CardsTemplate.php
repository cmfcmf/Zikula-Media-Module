<?php

namespace Cmfcmf\Module\MediaModule\CollectionTemplate;

class CardsTemplate extends AbstractTemplate
{
    public function getTitle()
    {
        return $this->__('Cards with thumbnails');
    }
}
