<?php

namespace Cmfcmf\Module\MediaModule\CollectionTemplate;

class SliderTemplate extends AbstractTemplate
{
    public function getTitle()
    {
        return $this->__('Big thumbnail slider');
    }
}
