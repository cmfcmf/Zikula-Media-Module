<?php

namespace Cmfcmf\Module\MediaModule\CollectionTemplate;

class GridTemplate extends AbstractTemplate
{
    public function getTitle()
    {
        return $this->__('Seamless and responsive grid');
    }
}
